from django.urls import path
from projects.views import create_project, project_detail, project_list


urlpatterns = [
    path("", project_list, name="list_projects"),
    path("<int:id>/", project_detail, name="show_project"),
    path("create/", create_project, name="create_project"),
]
